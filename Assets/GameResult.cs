﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// ゴール後の処理
/// 作成日：2018年12月26日
/// </summary>
public class GameResult : MonoBehaviour
{
    //フィールド
    private int highScore;　//ハイスコア
    public Text resultTime; //ゴール時のタイム
    public Text bestTime; //最速タイム
    public GameObject resultUI; //リザルト関連のUI

    // Start is called before the first frame update
    void Start()
    {
        //HighScoreという文字列があれば
        if (PlayerPrefs.HasKey("HighScore"))
        {
            //専用変数にHighScoreの項目に対応する数値(文字列)を整数型に変換して格納する
            highScore = PlayerPrefs.GetInt("HighScore");
        }
        else
        {
            highScore = 999;
        }
    }

    // Update is called once per frame
    void Update()
    {
        //ゴールフラグがONだったら
        if (Goal.goal)
        {
            //Result関連のUIをアクティブ(描画・更新)状態にする
            resultUI.SetActive(true);
            int result = Mathf.FloorToInt(Timer.time);
            resultTime.text = "ResultTime:" + result;
            bestTime.text = "BestTime:" + highScore;

            //highscore(過去のプレイ時のモノ)よりresultが小さい(速い)場合
            if(highScore > result)
            {
                //最高記録をresult(今回のプレイ)に置き換え、記録する
                PlayerPrefs.SetInt("HighScore", result);
            }
        }
    }

    /// <summary>
    /// リトライ処理
    /// </summary>
    public void OnRetry()
    {
        //現在動作中のシーンを再度ロード
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
