﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// ステージからの落下判定
/// 作成日：2018年12月26日
/// </summary>
public class Out : MonoBehaviour
{
    /// <summary>
    /// 衝突判定
    /// </summary>
    /// <param name="col">衝突対象のコライダー</param>
    public void OntriggerEnter(Collider col)
    {
        //衝突対象のtagがPlayerだったら
        if(col.gameObject.tag == "Player")
        {
            //現在動作中のシーンの名前(引数)を再度ロードする
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
