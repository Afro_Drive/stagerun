﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ゴール処理
/// </summary>
public class Goal : MonoBehaviour
{
    //フィールド
    public static bool goal;　//ゴールしたか？

    // Start is called before the first frame update
    void Start()
    {
        //最初はゴール状態でないとする
        goal = false;
    }

    void OnTriggerEnter(Collider col)
    {
        //接触対象のタグがPlayerだったら
        if(col.gameObject.tag == "Player")
        {
            //ゴール状態にする
            goal = true;
        }
    } 

    // Update is called once per frame
    void Update()
    {
        
    }
}
